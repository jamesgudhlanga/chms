<?php

/*AUTH*/
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'Api\Auth\AuthController@login');
    Route::post('logout', 'Api\Auth\AuthController@logout');
    Route::post('refresh', 'Api\Auth\AuthController@refresh');
    Route::post('me', 'Api\Auth\AuthController@me');
    Route::post('register', 'Api\Auth\RegisterController@register');
});

/*USERS*/
Route::group(['middleware' => ['jwt']],function () {
    Route::apiResource('users', 'Api\Users\UsersController');
    Route::group(['prefix' => 'users'], function () {
        Route::delete('{user}/destroy-permanently}', 'Api\Users\UsersController@destroyPermanently')
            ->name('users.destroy-permanently');
        Route::put('{user}/restore}', 'Api\Users\UsersController@restore')
            ->name('users.restore');
    });
});
