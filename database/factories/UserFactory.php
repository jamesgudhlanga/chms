<?php

use App\Models\Users\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'first_name' => $firstName = $faker->firstName,
        'middle_names' => $faker->firstName,
        'last_name' => $lastName = $faker->lastName,
        'display_name' => $firstName . ' ' . $lastName,
        'email' => $faker->safeEmail,
        'mobile_phone_number' => $faker->phoneNumber,
        'avatar' => $faker->url,
        'title_id' => $faker->numberBetween(1, 10),
        'gender_id' => $faker->numberBetween(1, 10),
        'status_id' => $faker->numberBetween(1, 5),
        'facebook' => $faker->url,
        'twitter' => $faker->url,
        'instagram' => $faker->url,
        'linkedin' => $faker->url,
        'biography' => $faker->paragraph(10),
        'email_verified_at' => now(),
        'password' => bcrypt('secret'),
        'remember_token' => Str::random(10),
    ];
});
