<?php

use Illuminate\Database\Seeder;

use App\Models\Users\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Empty the table
        DB::table('users')->delete();
        // Reset Auto Increment
        DB::statement('ALTER TABLE users AUTO_INCREMENT = 0');
        $data = [
            1 => [
                'first_name' => 'Chief',
                'middle_names' => 'Engineer',
                'last_name' => 'Developer',
                'mobile_phone_number' => '0614367071',
                'email' => 'jimmyneds@gmail.com',
                'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
                'remember_token' => str_random(10)
            ]
        ];

        foreach ($data as $row)
        {
            User::firstOrCreate($row);
        }
    }
}
