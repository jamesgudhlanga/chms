<?php

namespace Tests\Feature\Http\Controllers\Api\Users;

use App\Models\Users\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReadUserTest extends TestCase
{
    use RefreshDatabase;

    protected $user;
    protected $jsonStructure;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = create(User::class);
        $this->jsonStructure = ['id','first_name','middle_names','last_name','display_name','email','mobile_phone_number','avatar',
            'title_id','gender_id','status_id','facebook','twitter','instagram','linkedin','biography','email_verified_at',
            'created_at'];
    }

    /**
     * @test
     */
    public function that_non_authenticated_users_can_not_access_the_show_endpoint() {
        $this->withExceptionHandling()
            ->json('GET', "api/users/{$this->user->id}")
            ->assertStatus(400);
    }
    /**
     * @test
     */
    public function that_non_authenticated_users_can_not_access_the_index_endpoint() {
        $this->withExceptionHandling()
            ->json('GET', route('users.index'))
            ->assertStatus(400);
    }

    /**
     * @test
     */
    public function that_it_will_fail_with_a_404_if_user_not_found()
    {
        $this->withHeaders(['Authorization' => 'Bearer '. $this->authenticate()])
            ->json('GET', route('users.show', [-1]))
            ->assertStatus(404);
    }

    /**
     * @test
     */
    public function that_it_can_return_a_user_if_authenticated() {
        $this->withHeaders(['Authorization' => 'Bearer '. $this->authenticate()])
            ->json('GET', route('users.show', [$this->user->id]))
            ->assertStatus(200)
            ->assertJsonStructure($this->jsonStructure)
            ->assertJson([
                'first_name' => $this->user->first_name,
                'middle_names' => $this->user->middle_names,
                'last_name' => $this->user->last_name,
                'email' => $this->user->email,
                'mobile_phone_number' => $this->user->mobile_phone_number
            ]);
    }

    /**
     * @test
     */
    public function that_it_can_return_a_collection_of_paginated_users_if_authenticated() {
        $products = create(User::class,[], 3);
        $this->withHeaders(['Authorization' => 'Bearer '. $this->authenticate()])
            ->json('GET', route('users.index'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => $this->jsonStructure
                ],
                'links' => ['first','last','prev','next'],
                'meta' => ['current_page','last_page','from','to','path','per_page','total']
            ]);
    }
}
