<?php

namespace Tests\Feature\Http\Controllers\Api\Users;

use App\Models\Users\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteUserTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = create(User::class);
    }

    /**
     * @test
     */
    public function that_non_authenticated_users_can_not_access_the_delete_endpoint ()
    {
        $this->withExceptionHandling()
            ->json('DELETE', route('users.destroy', [$this->user->id]))
            ->assertStatus(400);
    }
    /**
     * @test
     */
    public function that_it_can_fail_with_404_if_user_not_found() {
        $this->withHeaders(['Authorization' => 'Bearer '. $this->authenticate()])
            ->json('DELETE', route('users.destroy', [-1]))
            ->assertStatus(404);
    }

    /**
     * @test
     */
    public function that_it_can_soft_delete_a_user_if_authenticated() {
        $this->withHeaders(['Authorization' => 'Bearer '. $this->authenticate()])
            ->json('DELETE', route('users.destroy', [$this->user->id]))
            ->assertStatus(204);
        $this->assertSoftDeleted('users', $this->user->toArray());
        $this->assertTrue($this->user->fresh()->trashed());
    }

    /**
     * @test
     */
    public function that__itcan_permanently_delete_a_user_if_authenticated() {
        $this->withHeaders(['Authorization' => 'Bearer '. $this->authenticate()])
            ->json('DELETE', route('users.destroy-permanently', [$this->user->id]))
            ->assertStatus(204);
        $this->assertDatabaseMissing('users', $this->user->toArray());
    }
}
