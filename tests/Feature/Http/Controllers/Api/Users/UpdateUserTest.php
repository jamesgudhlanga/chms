<?php

namespace Tests\Feature\Http\Controllers\Api\Users;

use App\Models\Users\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateUserTest extends TestCase
{
    use RefreshDatabase;

    protected $user;
    protected $userUpdate;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = create(User::class);
        $this->userUpdate = make(User::class);
    }

    /**
     * @test
     */
    public function that_non_authenticated_users_can_not_access_the_update_endpoint ()
    {
        $this->withExceptionHandling()
            ->json('PUT', route('users.update', [$this->user->id]), $this->userUpdate->toArray())
            ->assertStatus(400);
    }

    /**
     * @test
     */
    public function that_it_can_fail_with_a_404_if_user_not_found() {
        $this->withHeaders(['Authorization' => 'Bearer '. $this->authenticate()])
            ->json('PUT', route('users.update', [-1]), $this->userUpdate->toArray())
            ->assertStatus(404);
    }

    /**
     * @test
     */
    public function that_it_can_update_user_if_authenticated() {
        $this->withHeaders(['Authorization' => 'Bearer '. $this->authenticate()])
            ->json('PUT', route('users.update', [$this->user->id]), $this->userUpdate->toArray())
            ->assertJson([
                'first_name' => $this->userUpdate->first_name,
                'middle_names' => $this->userUpdate->middle_names,
                'last_name' => $this->userUpdate->last_name,
                'email' => $this->userUpdate->email,
                'mobile_phone_number' => $this->userUpdate->mobile_phone_number
            ])
            ->assertStatus(200);

        $this->assertDatabaseHas('users', [
            'first_name' => $this->userUpdate->first_name,
            'middle_names' => $this->userUpdate->middle_names,
            'last_name' => $this->userUpdate->last_name,
            'email' => $this->userUpdate->email,
            'mobile_phone_number' => $this->userUpdate->mobile_phone_number]);
    }

    /**
     * @test
     */
    public function that_it_can_restore_a_soft_deleted_user_if_authenticated() {
        //Soft delete user
        $this->withHeaders(['Authorization' => 'Bearer '. $this->authenticate()])
            ->json('DELETE', route('users.destroy', [$this->user->id]))
            ->assertStatus(204);
        $this->assertSoftDeleted('users', $this->user->toArray());
        //then restore
        $this->withHeaders(['Authorization' => 'Bearer '. $this->authenticate()])
            ->json('PUT', route('users.restore', [$this->user->id]))
            ->assertStatus(200);
        $this->assertTrue(!($this->user->fresh()->trashed()));
    }

    /**
     * @test
     */
    public function that_first_name_is_required_on_update()
    {
        $this->patchUser(['first_name' => null])
            ->assertStatus(422)
            ->assertExactJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'first_name' => ['The first name field is required.']
                ]
            ]);
    }

    /**
     * @test
     */
    public function that_last_name_is_required_on_update()
    {
        $this->patchUser(['last_name' => null])
            ->assertStatus(422)
            ->assertExactJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'last_name' => ['The last name field is required.']
                ]
            ]);
    }

    /**
     * @test
     */
    public function that_email_is_required_on_update()
    {
        $this->patchUser(['email' => null])
            ->assertStatus(422)
            ->assertExactJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'email' => ['The email field is required.']
                ]
            ]);
    }

    /**
     * @test
     */
    public function that_email_is_unique_on_update()
    {
        $user = create(User::class);
        $this->patchUser(['email' => $user->email])
            ->assertStatus(422)
            ->assertExactJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'email' => ['The email has already been taken.']
                ]
            ]);
    }

    /**
     * @test
     */
    public function that_mobile_phone_number_is_required_on_update()
    {
        $this->patchUser(['mobile_phone_number' => null])
            ->assertStatus(422)
            ->assertExactJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'mobile_phone_number' => ['The mobile phone number field is required.']
                ]
            ]);
    }

    /**
     * @test
     */
    public function that_mobile_phone_number_is_unique_on_update()
    {
        $user = create(User::class);
        $this->patchUser(['mobile_phone_number' => $user->mobile_phone_number])
            ->assertStatus(422)
            ->assertExactJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'mobile_phone_number' => ['The mobile phone number has already been taken.']
                ]
            ]);
    }

    protected function patchUser($overrides = [])
    {
        $user = make(User::class, $overrides);
        return $this->withHeaders(['Authorization' => 'Bearer '. $this->authenticate()])
            ->withExceptionHandling()
            ->json('PUT', route('users.update', [$this->user->id]), $user->toArray());
    }
}
