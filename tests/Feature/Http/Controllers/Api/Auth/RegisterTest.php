<?php

namespace Tests\Feature\Http\Controllers\Api\Auth;

use App\Models\Users\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    protected $postUser;

    public function setUp(): void
    {
        parent::setUp();
        $this->postUser = make(User::class);
    }

    /**
     * @test
     */
    public function that_a_user_can_register()
    {
        $this->json('POST', 'api/auth/register', $this->postUser->toArray())
            ->assertJsonStructure([
                'id', 'first_name', 'middle_names', 'last_name', 'display_name', 'email', 'mobile_phone_number', 'avatar',
                'title_id', 'gender_id', 'status_id', 'facebook', 'twitter', 'instagram', 'linkedin', 'biography', 'email_verified_at',
                'created_at'
            ])
            ->assertJson([
                'first_name' => $this->postUser->first_name,
                'middle_names' => $this->postUser->middle_names,
                'last_name' => $this->postUser->last_name,
                'email' => $this->postUser->email,
                'mobile_phone_number' => $this->postUser->mobile_phone_number
            ])
            ->assertStatus(201);

        $this->assertDatabaseHas('users', [
            'first_name' => $this->postUser->first_name,
            'middle_names' => $this->postUser->middle_names,
            'last_name' => $this->postUser->last_name,
            'email' => $this->postUser->email,
            'mobile_phone_number' => $this->postUser->mobile_phone_number
        ]);
    }

    /**
     * @test
     */
    public function that_first_name_is_required()
    {
        $this->postUser(['first_name' => null])
            ->assertStatus(422)
            ->assertExactJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'first_name' => ['The first name field is required.']
                ]
            ]);
    }

    /**
     * @test
     */
    public function that_last_name_is_required()
    {
        $this->postUser(['last_name' => null])
            ->assertStatus(422)
            ->assertExactJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'last_name' => ['The last name field is required.']
                ]
            ]);
    }

    /**
     * @test
     */
    public function that_email_is_required()
    {
        $this->postUser(['email' => null])
            ->assertStatus(422)
            ->assertExactJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'email' => ['The email field is required.']
                ]
            ]);
    }

    /**
     * @test
     */
    public function that_email_is_unique()
    {
        $user = create(User::class);
        $this->postUser(['email' => $user->email])
            ->assertStatus(422)
            ->assertExactJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'email' => ['The email has already been taken.']
                ]
            ]);
    }

    /**
     * @test
     */
    public function that_mobile_phone_number_is_required()
    {
        $this->postUser(['mobile_phone_number' => null])
            ->assertStatus(422)
            ->assertExactJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'mobile_phone_number' => ['The mobile phone number field is required.']
                ]
            ]);
    }

    /**
     * @test
     */
    public function that_mobile_phone_number_is_unique()
    {
        $user = create(User::class);
        $this->postUser(['mobile_phone_number' => $user->mobile_phone_number])
            ->assertStatus(422)
            ->assertExactJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'mobile_phone_number' => ['The mobile phone number has already been taken.']
                ]
            ]);
    }

    protected function postUser($overrides = [])
    {
        $user = make(User::class, $overrides);
        return $this->withExceptionHandling()
            ->json('POST', 'api/auth/register', $user->toArray());
    }
}
