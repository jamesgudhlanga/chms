<?php

namespace Tests\Feature\Http\Controllers\Api\Auth;

use App\Models\Users\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = create(User::class);
    }

    /**
     * @test
     */
    public function it_can_login_and_return_a_token() {
        $response = $this->json('POST','api/auth/login',[
            'email' => $this->user->email,
            'password' => 'secret',
        ])->assertStatus(200);
        $this->assertArrayHasKey('access_token',$response->json());
    }
}
