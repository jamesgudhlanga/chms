<?php

namespace App\Http\Resources\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'middle_names' => $this->middle_names,
            'last_name' => $this->last_name,
            'display_name' => $this->display_name,
            'email' => $this->email,
            'mobile_phone_number' => $this->mobile_phone_number,
            'avatar' => $this->avatar,
            'title_id' => $this->title_id,
            'gender_id' => $this->gender_id,
            'status_id' => $this->status_id,
            'facebook' => $this->facebook,
            'twitter' => $this->twitter,
            'instagram' => $this->instagram,
            'linkedin' => $this->linkedin,
            'biography' => $this->biography,
            'email_verified_at' => $this->email_verified_at,
            'created_at' => (string)$this->created_at,
        ];
    }
}
