<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Requests\Users\UserUpdateRequest;
use App\Http\Resources\Users\UserCollection;
use App\Http\Resources\Users\UserResource;
use App\Models\Users\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{

    public function __construct()
    {

    }


    public function index()
    {
        $users = User::paginate();
        return new UserCollection($users);
    }

    public function store(Request $request)
    {

    }

    public function show($user)
    {
        try {
            $user = User::findOrFail($user);
            return response()->json(new UserResource($user));
        }
        catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 404);
        }
    }


    public function update(UserUpdateRequest $request, $id)
    {
        try {
            $user = User::findOrFail($id);
            $user->update([
                'first_name' => $request->first_name,
                'middle_names' => $request->middle_names,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'mobile_phone_number' => $request->mobile_phone_number,
            ]);
            return response()->json(new UserResource($user));
        }
        catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 404);
        }
    }

    public function restore($id) {
        try {
            $user = User::withTrashed()->findOrFail($id);
            $user->restore();
            return response()->json(['message' => 'User successfully restored'], 200);
        }
        catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 404);
        }
    }

    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();
            return response()->json(['message' => 'User successfully deleted'], 204);
        }
        catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 404);
        }
    }

    public function destroyPermanently($id) {
        try {
            $user = User::findOrFail($id);
            $user->forceDelete();
            return response()->json(['message' => 'User successfully permanently deleted'], 204);
        }
        catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 404);
        }
    }
}
