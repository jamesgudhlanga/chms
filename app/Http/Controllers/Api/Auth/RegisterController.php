<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Requests\Users\UserStoreRequest;
use App\Http\Resources\Users\UserResource;
use App\Models\Users\User;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    public function register(UserStoreRequest $request) {
        $user = User::create([
           'first_name' => $request->first_name,
           'middle_names' => $request->middle_names,
           'last_name' => $request->last_name,
            'email' => $request->email,
            'mobile_phone_number' => $request->mobile_phone_number,
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
            'email_verified_at' => $request->email_verified_at,
            'remember_token' => $request->remember_token
        ]);

        return response()->json(new UserResource($user), 201);
    }
}
